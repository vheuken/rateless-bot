(ns rateless-bot.commands
  (:require [event-action-lib.core :as eal]
            [clojure.core.async :as a]
            [clojure.string :as string]
            [clj-http.client :as http]
            [cheshire.core :as json]
            [rateless-bot.state :as state]
            [discljord.messaging :as messaging]
            [rateless-bot.commands.emotes]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]))

;;;
;;; Actions
;;;

(eal/reg-action! {:id :create-message
                  :execute-fn
                  (fn [{:keys [channel content file]}]
                    (messaging/create-message! @state/messaging-ch channel :content content :file file))})

(eal/reg-action! {:id :delete-message
                  :execute-fn
                  (fn [{:keys [channel message]}]
                    (messaging/delete-message! @state/messaging-ch channel message))})

;;;
;;; Inputs
;;;

(defn get-current-weather [location]
  (-> "https://api.openweathermap.org/data/2.5/weather?q="
      (str location
           "&APPID="
           (:open-weather-map-api-key @state/config))
      (http/get {:accept :json})
      :body
      (json/parse-string true)))

(eal/reg-input! {:id :current-weather
                 :eval-fn
                 (fn [{:keys [params]}]
                   (let [location (apply str params)]
                     (get-current-weather location)))})

;;;
;;; Events
;;;

(eal/reg-event! {:id :!ping
                 :handler-fn
                 (fn [{:keys [data]}]
                   {:create-message {:channel (:channel-id (:event-data data))
                                     :content "PONG!"}})})

(defn kelvin->celcius [k]
  (- k 273.15))

(defn kelvin->fahrenheit [k]
  (- (* k (/ 9 5)) 459.67))

(eal/reg-event! {:id :!weather
                 :inputs [:current-weather]
                 :handler-fn
                 (fn [{:keys [data inputs]}]
                   (let [current-weather (:current-weather inputs)
                         temp (:temp (:main current-weather))
                         weather (str "Current weather for "
                                      (:name current-weather) ", " (:country (:sys current-weather))
                                      "\n"
                                      (:description (first (:weather current-weather))) "; "
                                      (int (kelvin->celcius temp)) "°C ("
                                      (int (kelvin->fahrenheit temp)) "°F)")]
                     {:create-message {:channel (:channel-id (:event-data data))
                                       :content weather}}))})

(defn get-uptime-ms []
  (.getUptime (java.lang.management.ManagementFactory/getRuntimeMXBean)))

(defn generate-uptime-message [milliseconds-uptime]
  (let [milliseconds-in-a-minute (* 60 1000)
        milliseconds-in-an-hour (* 60 milliseconds-in-a-minute)
        milliseconds-in-a-day (* 24 milliseconds-in-an-hour)
        days (int (/ milliseconds-uptime milliseconds-in-a-day))
        hours (int (/ (- milliseconds-uptime (* days milliseconds-in-a-day))
                      (* 60 60 1000)))
        minutes (int (/ (- milliseconds-uptime (* hours milliseconds-in-an-hour))
                        (* 60 1000)))
        seconds (float (/ (- milliseconds-uptime (* minutes milliseconds-in-a-minute))
                          1000))]
    (str "Uptime: "
         days (if (= 1 days) " day, " " days, ")
         hours (if (= 1 hours) " hour, " " hours, ")
         minutes (if (= 1 minutes) " minute, " " minutes, ")
         seconds (if (= 1 seconds) " second ("  " seconds (")
         milliseconds-uptime " ms).")))

(eal/reg-event! {:id :!uptime
                 :handler-fn
                 (fn [{:keys [data inputs]}]
                   {:create-message {:channel (-> data :event-data :channel-id)
                                     :content (generate-uptime-message (get-uptime-ms))}})})

(def version "0.2.1")
(def about-message (str "RatelessBot v" version "\n"
                        "https://gitlab.com/vheuken/rateless-bot" "\n"
                        (generate-uptime-message (get-uptime-ms))))

(eal/reg-event! {:id :!about
                 :handler-fn
                 (fn [{:keys [data inputs]}]
                   {:create-message {:channel (-> data :event-data :channel-id)
                                     :content about-message}})})


;;;
;;; Dispatcher
;;;

(defmulti handle-event
  (fn [event-type event-data]
    event-type))

(defmethod handle-event :default
  [event-type event-data])

(def commands #{:!weather :!emote :!ping :!uptime :!about})

(defn drop-namespace-from-content [content]
  (if (namespace (keyword content))
    (second (string/split content #"/" 2))
    content))

(defn get-command-from-content
  ([content]
   (when (= (nth content 0) \!)
     (let [command-keyword (keyword (first (string/split content #" ")))]
       (when-not (namespace command-keyword)
         command-keyword))))
  ([content *ns*]
   (if *ns*
     (when (= *ns* (namespace (keyword content)))
       (let [sanitized-content (drop-namespace-from-content content)]
         (get-command-from-content sanitized-content)))
     (get-command-from-content content))))

(defmethod handle-event :message-create
  [event-type event-data]
  (let [content (:content event-data)]
    (info "Message received: \"" content "\"")
    (when-let [command (get-command-from-content content (:commands-ns @state/config))]
      (let [sanitized-content (drop-namespace-from-content content)]
        (if (contains? commands command)
          (eal/dispatch! [command {:params (rest (string/split sanitized-content #" "))
                                   :event-data event-data}])
          (eal/dispatch! [:!emote {:params (string/split (subs sanitized-content 1) #" ")
                                   :event-data event-data}]))))))
