(ns rateless-bot.state)

(def config (atom {}))
(defonce state (atom nil))
(defonce messaging-ch (atom nil))
(defonce connection-ch (atom nil))
