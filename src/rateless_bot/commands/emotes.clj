(ns rateless-bot.commands.emotes
  (:require [event-action-lib.core :as eal]
            [clojure.string :as string]
            [clj-http.client :as http]
            [clojure.java.io :as io]))

(def emote-name-character-limit 1000)

(eal/reg-input! {:id :emotes
                 :eval-fn
                 (fn [data]
                   (->> (clojure.java.io/file "emotes")
                        file-seq
                        rest
                        (map #(hash-map :file %
                                        :name  (string/lower-case (first (string/split (.getName %) #"\.")))))))})

(defn generate-no-emote-message [emote-name]
  (str "Emote \"" emote-name "\" does not exist. Add it with `!emote add " emote-name "`"))

(defn display-emote [{:keys [emote channel-id message author-username]}]
  (if emote
    {:create-message {:channel channel-id
                      :content (str "**" author-username "**: " (:content message))
                      :file (:file emote)}
     :delete-message {:channel channel-id
                      :message (:id message)}}
    {:create-message {:channel channel-id
                      :content (generate-no-emote-message (:name emote))}}))

(defn add-emote [{:keys [emote-name emote-url channel-id]}]
  (if (< (count emote-name) emote-name-character-limit)
    (if emote-url
      [[:add-emote {:emote-name emote-name
                    :emote-url emote-url
                    :channel-id channel-id}]
       [:create-message {:channel channel-id
                         :content (str "Emote added! Try it with `!" emote-name "`")}]]
      {:create-message {:channel channel-id
                        :content "No image provided. Please try again with an image attached."}})
    {:create-message {:channel channel-id
                      :content (str "That emote name is too long! Please use a name with fewer than "
                                     emote-name-character-limit " characters.")}}))

(eal/reg-event! {:id :!emote
                 :inputs [:emotes]
                 :handler-fn
                 (fn [{:keys [data inputs]}]
                   (let [params (:params data)
                         first-param (string/lower-case (first params))
                         channel-id (-> data :event-data :channel-id)]
                     (if (= first-param "add")
                       (add-emote {:emote-name (second params)
                                   :emote-url (-> data :event-data :attachments first :url)
                                   :channel-id channel-id})
                       (display-emote {:emote (first (filter #(= first-param (:name %)) (:emotes inputs)))
                                       :author-username (-> data :event-data :author :username)
                                       :message (select-keys (:event-data data) [:id :content])
                                       :channel-id channel-id}))))})

(eal/reg-action! {:id :add-emote
                  :execute-fn
                  (fn [{:keys [emote-name emote-url channel-id]}]
                    (with-open [emote-source-stream (:body (http/get emote-url {:as :stream}))]
                      (let [file-extension (org.apache.commons.io.FilenameUtils/getExtension emote-url)
                            emote-destinaton-file (io/file (str "emotes/" emote-name "." file-extension))]
                        (io/copy emote-source-stream emote-destinaton-file))))})
