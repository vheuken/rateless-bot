(ns rateless-bot.core
  (:require [discljord.connections :as connections]
            [discljord.messaging :as messaging]
            [discljord.events :as events]
            [rateless-bot.commands :as commands]
            [rateless-bot.state :as state]
            [clojure.core.async :as async])
  (:gen-class))

(defn reload-config! []
  (reset! state/config (read-string (slurp "config.edn"))))

(defn start-bot! []
  (reload-config!)
  (future
    (let [token (:token @state/config)
          event-ch (async/chan 100)
          init-state {:connection state/connection-ch
                      :event event-ch
                      :messaging state/messaging-ch}]
      (reset! state/messaging-ch (messaging/start-connection! token))
      (reset! state/connection-ch (connections/connect-bot! token event-ch))
      (reset! state/state init-state)
      (events/message-pump! event-ch commands/handle-event)
      (messaging/stop-connection! @state/messaging-ch)
      (connections/disconnect-bot! @state/connection-ch))))

(defn stop-bot! []
  (async/put! @(:connection @state/state) [:disconnect]))


(defn -main [& args]
  (start-bot!))
