(defproject rateless-bot "0.2.1"
  :description "Just a Discord bot."
  :url "https://gitlab.com/vheuken/rateless-bot"
  :license {:name "zlib/libpng License"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [event-action-lib "0.0.1"]
                 [org.suskalo/discljord "0.2.4"]
                 [clj-http "3.9.1"]
                 [cheshire "5.8.1"]
                 [com.taoensso/timbre "4.10.0"]]

  :main ^:skip-aot rateless-bot.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
